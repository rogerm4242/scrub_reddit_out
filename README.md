![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

## Scrub reddit out link

This simple web page allows you to paste a reddit out link into an input box.
The plain link is then generated below.

